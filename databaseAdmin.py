import mysql.connector
from datetime import datetime
from mysql.connector import errorcode


def connectToDatabase():
	DATABASE = 'accloud'
	try:
	  cnx = mysql.connector.connect(user='accloud', password='p08MYRoXj64INNRi',
	  								host='127.0.0.1', database=DATABASE)
	  #print("Connected to Database " + DATABASE)
	  return cnx;
	  
	except mysql.connector.Error as err:
	  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
		  print ("Something is wrong with your user name or password")
	  elif err.errno == errorcode.ER_BAD_DB_ERROR:
		  print("Database does not exist")
	  else:
		  print(err)
	else:
		cnx.close()


""" ==================================== INSERTS ==================================== """
def sql_insertCashPayment(idPurchase, CashInserted, BillsPaid, LowerBillValue, UpperBillValue, LowerBillQuantity, UpperBillQuantity, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertCashPayment',(idPurchase, CashInserted, BillsPaid, LowerBillValue, UpperBillValue, LowerBillQuantity, UpperBillQuantity))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertCashPayment2(idPurchase, CashInserted, CassetteValue1, CassetteValue2, CassetteValue3, CassetteValue4, CassetteQty1, CassetteQty2, CassetteQty3, CassetteQty4, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertCashPayment2',(idPurchase, CashInserted, CassetteValue1, CassetteValue2, CassetteValue3, CassetteValue4, CassetteQty1, CassetteQty2, CassetteQty3, CassetteQty4))
	cnx.commit()
	cursor.close()
	return 1
	
	
def sql_insertCreditCardPayment(idPurchase, cardHolder, last4Numbers,cnx):
	#print "insertCreditCardPayment",(idPurchase, cardHolder, last4Numbers)
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('insertCreditCardPayment',(idPurchase ,cardHolder,last4Numbers))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertReceiptData(idPurchase, datetime, receiptData, cnx):
	#print "insertReceiptData", (idPurchase, datetime, receiptData)
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertReceiptData',(idPurchase, datetime, receiptData))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertPackage(idPurchase, packType, quantity, total, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertNewPackage',(idPurchase, packType, quantity, total))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertItem(idPurchase, itemName, itemPrice, QtyDispensed, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertItemInput',(idPurchase, itemName, itemPrice, QtyDispensed))
	cnx.commit()
	cursor.close()
	return 1

def sql_insertHopper(idPurchase, hopperName, HopperValue, QtyDispensed, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertHopperInput',(idPurchase, hopperName, HopperValue, QtyDispensed))
	
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertCassette(idPurchase, cassetteName, cassetteValue, QtyDispensed, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertCassetteInput',(idPurchase, cassetteName, cassetteValue, QtyDispensed))
	
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertGateAdmission(idPurchase, gaType, quantity, total, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertNewGateAdmission',(idPurchase, gaType, quantity, total))
	cnx.commit()
	cursor.close()
	return 1

def sql_insertGameCard(idPurchase, CreditsAdded, prevBalance, newBalance, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertNewGameCard',(idPurchase, CreditsAdded, prevBalance, newBalance))
	cnx.commit()
	cursor.close()
	return 1

def sql_insertWristband(idPurchase, wbType, qty, totalCash, codes,cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertNewWristband',(idPurchase, wbType, qty, totalCash, codes))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertCashMeter(idPurchase, cashInserted, changeDispensed, netGain, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertCashMeter',(idPurchase, cashInserted, changeDispensed, netGain))
	cnx.commit()
	cursor.close()
	return 1
		
def sql_insertPurchase(IdKiosk, pNumber, PurchaseAm, datetime, PayType, purchaseStatus, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertNewPurchase',(IdKiosk, pNumber, PurchaseAm, datetime, PayType, purchaseStatus))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_insertNewKioskWithMac(opName, kName, serialN, ipAddr, Notes, macAddr, machineType, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertNewKioskWithMac',(opName, kName, serialN, ipAddr, Notes, macAddr, machineType))
	cnx.commit()
	cursor.close()
	return 1

def sql_insertOrUpdateMachineStatus(i_MachineID, i_SoftwareVersion, i_System, i_carwash, i_CardDispenser1, 
	i_CardDispenser2, i_CardDispenser3, i_CardDispenser4, i_tickDisp1, i_tickDisp2, i_tickDisp3, i_tickDisp4,
	i_Hopper1, i_Hopper2, i_Hopper3, i_Hopper4, i_Validator1, i_Validator2, i_CoinAcceptor, i_CreditCardSystem,
	i_BillDispenser, i_billCst1, i_billCst2, i_billCst3, i_billCst4, i_billReject, i_billMotor, i_billQual,
	i_billDiv, i_billStack, i_billPres, i_billData, i_display, i_program, i_printer1, i_printer2, i_temp, 
	i_volt, i_door, i_UpdateTime, cnx):
	
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('insertOrUpdateMachineStatus',(i_MachineID, i_SoftwareVersion, i_System, i_carwash, i_CardDispenser1, 
	i_CardDispenser2, i_CardDispenser3, i_CardDispenser4, i_tickDisp1, i_tickDisp2, i_tickDisp3, i_tickDisp4,
	i_Hopper1, i_Hopper2, i_Hopper3, i_Hopper4, i_Validator1, i_Validator2, i_CoinAcceptor, i_CreditCardSystem,
	i_BillDispenser, i_billCst1, i_billCst2, i_billCst3, i_billCst4, i_billReject, i_billMotor, i_billQual,
	i_billDiv, i_billStack, i_billPres, i_billData, i_display, i_program, i_printer1, i_printer2, i_temp, 
	i_volt, i_door, i_UpdateTime))
	cnx.commit()
	cursor.close()
	return 1

""" ==================================== UPDATES ==================================== """

def sql_editKioskMacAddress(IdKiosk, macAddr, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('editKioskMacAddress',(IdKiosk, macAddr))
	cnx.commit()
	cursor.close()
	return 1
	
def sql_updateHardMeter(IdKiosk, HardMeter, cnx):
	cursor = cnx.cursor(buffered=True)

	cursor.callproc('updateHardMeter',(IdKiosk, HardMeter))
	cnx.commit()
	cursor.close()
	return 1

""" ==================================== GETTERS ==================================== """
def sql_checkSerialNumber(serialNumber, datetime, kID, cnx ):
	#print "checkSerialNumber", (serialNumber, datetime, kID)
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('checkSerialNumber',(serialNumber, datetime, kID))
	
	for R in cursor.stored_results():
		pSerial = R.fetchall()	
	cursor.close()
	if (pSerial):
		return True
	else:
		return False

def sql_checkSerialNumberBalance(serialNumber, datetime, kID, cnx ):
	#print "checkSerialNumberBalance", (serialNumber, datetime, kID)
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('checkSerialNumberBalance',(serialNumber, datetime, kID))
	
	for R in cursor.stored_results():
		pSerial = R.fetchall()	
	cursor.close()
	if (pSerial):
		return int(pSerial[0][0])
	else:
		return "None"
	
def sql_getPurchaseId(cnx):
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('getLatestPurchase',())
	
	for R in cursor.stored_results():
		Id = R.fetchall()	
	cursor.close()
	return int(Id[0][0])
	
def sql_getKioskIdbyIp(kioskIp, cnx):
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('getKioskIdbyIp',(kioskIp,))
	
	for R in cursor.stored_results():
		Id = R.fetchall()	
	cursor.close()
	if (Id):
		return int(Id[0][0])
	else:
		return "None"
		
def sql_getKioskIdbyMac(kioskMac, cnx):
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('getKioskIdbyMac',(kioskMac,))
	
	for R in cursor.stored_results():
		Id = R.fetchall()	
	cursor.close()
	if (Id):
		return int(Id[0][0])
	else:
		return "None"
		
def sql_getKioskIdByNameAndOpId(kName, opId, cnx):
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('getKioskIdByNameAndOpId',(kName, opId,))
	
	for R in cursor.stored_results():
		Id = R.fetchall()
	cursor.close()
	if (Id):
		return int(Id[0][0])
	else:
		return "None"
		
def sql_getOperatorIdByName(opName, cnx):
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('getOperatorIdByName',(opName,))
	
	for R in cursor.stored_results():
		Id = R.fetchall()
	cursor.close()
	if (Id):
		return int(Id[0][0])
	else:
		return "None"
		

def sql_getMachineTypeById(kioskId, cnx):
	cursor = cnx.cursor(buffered=True)
	cursor.callproc('getMachineTypeById',(kioskId,))
	
	for R in cursor.stored_results():
		Id = R.fetchall()
	cursor.close()
	if (Id):
		return int(Id[0][0])
	else:
		return "None"
		
#cnx = connectToDatabase()	
#print (sql_getPurchaseId(cnx))
