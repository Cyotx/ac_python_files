def cleanArray(array):
	while True:
		try:
			array.remove('')
		except:
			break
	return array
	
def removeSpaces(string):
	temp = cleanArray(string.split('  '))
	result = ""
	if len(temp) > 1:
		result = temp[0] + " " + temp[1]
	else:
		result = temp[0]
	return result
	
def removeStringSpaces(string):
	temp = cleanArray(string.split(' '))
	result = ""
	if len(temp) > 1:
		for i in temp:
			if i != "":
				result = result + i + " "
	else:
		result = temp[0]
	return result
	
def reArrange(array):
	stringResult = ""
	for i in array:
		i = removeSpaces(i)
		stringResult = stringResult + i + "|"
	return stringResult
	
def dh_processReceiptData(rawReceipt):
	cardCC = rawReceipt.split('|')
	if len(cardCC) == 1:
		cardCC = rawReceipt.split('  ')
	#print cardCC
	#print len(cardCC)
	cardCC = cleanArray(cardCC)

	return reArrange(cardCC)

