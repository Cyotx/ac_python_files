""" 
TODO list
Define error code. Where are the files failing to be uploaded? why so many fails?
Stuck after entry file... why? does it recover???
Error en todos!!! 
TypeError: 'not enough arguments for format string'
IndexError: 'list index out of range'


"""
#import pdb
import os.path 
import os
from databaseAdmin import *
from dataHelper import *
import time
import shutil
import datetime
import logging
logging.basicConfig(filename='log.log',level=logging.DEBUG)

DATA_PATH = '/home/accloud/'
BACKUP_PATH = '/home/accloud/Backup'
QUARANTINE_PATH = '/home/accloud/QuarantineFiles'
DEBUG_PATH = '/home/accloud/Debug'

MAX_BKUP_LENGHT = 10

ABORTED_LINES = 0

def reduceDataEntry(entry, lenght):
	iteration = lenght - 35
	while (iteration > 0):
		entry[34] += "-" + entry.pop()
		iteration -= 1
	return entry
	
def parseDate(date):
	dateList = date.split("/")
	try:
		date = dateList[2]+"-"+ dateList[0]+"-"+ dateList[1]
	except:
		date = "Date error"
	return date
	
def parseCardHolder(holder):
	parseList = holder.split("/")
	result = ""
	if len(parseList) == 1:
		result = parseList[0]
	elif len(parseList) == 2:
		result = parseList[1] + " " + parseList[0]
	elif len(parseList) == 2:
		result = parseList[1] + " " + parseList[2] + " " + parseList[0]
	return removeStringSpaces(result)
	
def parseTime(mytime):
	if len(mytime) == 8:
		if (mytime[6:] == 'PM'):
			if (int(mytime[:2]) == 12):
				mytime = str(int(mytime[:2]))+mytime[2:5]
			else:
				mytime = str(int(mytime[:2])+12)+mytime[2:5]
		else:
			mytime = mytime[:5]
			
	elif len(mytime) == 7:
		if (mytime[5:] == 'PM'):
			mytime = str(int(mytime[:1])+12)+mytime[1:4]
		else:
			mytime = mytime[:5]
			
	elif len(mytime) == 10:
		if (mytime[8:] == 'PM'):
			mytime = str(int(mytime[:1])+12)+mytime[1:7]
		else:
			mytime = mytime[:8]
	elif len(mytime) == 11:
		if (mytime[9:] == 'PM'):
			if (int(mytime[:2]) == 12):
				mytime = str(int(mytime[:2]))+mytime[2:8]
			else:
				mytime = str(int(mytime[:2])+12)+mytime[2:8]
		else:
			mytime = mytime[:8]	

	return mytime

	
	
def checkAndInsertItems(idPurchase, ItemNames, ItemPrices, ItemQtys, cnx):
	# this depends on the way that items are inserted
	for i in range(4):
		if ( int(ItemQtys[i]) != 0 and (ItemQtys[i] != '?') ):
			sql_insertItem(idPurchase, ItemNames[i], ItemPrices[i], ItemQtys[i], cnx)
	
def checkAndInsertHoppers(idPurchase, HopperNames, HopperValues, HopperQtys, cnx):
	# this depends on the way that hoppers are inserted
	for i in range(4):
		if ( int(HopperQtys[i]) != 0 and (HopperQtys[i] != '?') ):
			sql_insertHopper(idPurchase, HopperNames[i] +"-$"+ HopperValues[i], HopperValues[i], HopperQtys[i], cnx)
			
def checkAndInsertCassettes(idPurchase, CassetteValues, CassetteQtys, cnx):
	# this depends on the way that cassettes are inserted
	for i in range(4):
		if ( int(CassetteQtys[i]) != 0 and (CassetteQtys[i] != '?') ):
			sql_insertCassette(idPurchase, "Cassette-"+ CassetteValues[i], int(CassetteValues[i][1:]), CassetteQtys[i], cnx)
			
def CheckCassetteEntry(entry):
	if (entry != '?'):
		return entry
	else:
		return '0'
	
	
def checkAndInsertPackages(idPurchase,PackageQ, PackageTotal, cnx):
	for i in range(5):
		if (int(PackageQ[i]) != 0):
			sql_insertPackage(idPurchase, i+1, PackageQ[i], PackageTotal[i], cnx)
	#print "Packages inserted"
	
def checkAndInsertWristBand(idPurchase, wbQ, wbTotal, WBCodes, cnx):
	for i in range(2):
		if (int(wbQ[i]) != 0):
			sql_insertWristband(idPurchase, i+1, wbQ[i], wbTotal[i], WBCodes,cnx)
	#print "Wristband inserted"

def checkAndInsertGateAdmission(idPurchase, gaQ, gaTotal, cnx):
	for i in range(2):
		if (int(gaQ[i]) != 0):
			sql_insertGateAdmission(idPurchase, i+1, gaQ[i], gaTotal[i], cnx)
	#print "Gate Admission inserted"

# get the first line of the file and parse it into the Machine Status command
def collectAndInsertMachineStatus(idMachine, filepath, filedate, cnx):

	StatusList = txt_getFirstLine(filepath)
	
	if len(StatusList) < 38:
		print ("Status data is corrupted, aborting update operation")
		return 0
	else:
		for entry in StatusList:
			if entry == '':
				print ("Status data is corrupted, aborting update operation")
				return 0
		sql_insertOrUpdateMachineStatus(idMachine, StatusList[0], StatusList[1], StatusList[2], StatusList[3], StatusList[4],
		StatusList[5], StatusList[6], StatusList[7], StatusList[8], StatusList[9], StatusList[10], StatusList[11], StatusList[12],
		StatusList[13], StatusList[14], StatusList[15], StatusList[16], StatusList[17], StatusList[18], StatusList[19], StatusList[20],
		StatusList[21], StatusList[22], StatusList[23], StatusList[24], StatusList[25], StatusList[26], StatusList[27], StatusList[28],
		StatusList[29], StatusList[30], StatusList[31], StatusList[32], StatusList[33], StatusList[34], StatusList[35], StatusList[36],
		StatusList[37].split('\r')[0], filedate, cnx)

def getTotalCashInserted(string):
	try :
		result = int(string.split(':')[1])
	except:
		result = 0
	return result
	
def getTotalChangeDispensed(string):
	try :
		result = int(string.split(':')[2])
	except:
		result = 0
	return result
	

""" Review and organize Entry data for database insertion
	Depending on the machine type this will act differently
	@param	idkiosk 		- Kiosk to insert data to
	@param  entry   		- Array holding all data
	@param  purchaseStatus	- Complete or incomplete
	@param  dataVersion     - Dataset version (according to machine type)
	@param	cnx				- pointer to database conection object
"""
def doStuff(idkiosk, entry, purchaseStatus, dataVersion, cnx):
	
	# Data Version 1 - machine type 1: Kiosk
	if (dataVersion == 1):
		# Calculate purchase amount values
		## hacer manejo de excepciones.
		try:
			PackageQ=[float(entry[12]),float(entry[13]),float(entry[14]),float(entry[15]),float(entry[16])]
		except:
			PackageQ=[0,0,0,0,0]
		try:
			PackageTotal=[float(entry[7])*PackageQ[0],float(entry[8])*PackageQ[1],float(entry[9])*PackageQ[2],float(entry[10])*PackageQ[3],float(entry[11])*PackageQ[4]]
		except:
			PackageTotal=[0,0,0,0,0]
		# wrisbands, 0 = adult, 1 = child
		try:
			wbQ = [float(entry[19]), float(entry[20])] 
		except:
			wbQ = [0,0]
		try:
			wbTotal = [float(entry[17]), float(entry[18])]
		except:
			wbTotal = [0,0]
		try:
			WBCodes = entry[34]
		except:
			WBCodes = ""
		#print "2"
		# Gate Admission, 0 = A, 1 = B
		try:
			gaQ = [float(entry[30]), float(entry[31])]
		except:
			gaQ = [0,0]
		try: 
			gaTotal = [float(entry[32]), float(entry[33])] 
		except:
			gaTotal = [0,0]
		# get date and time, fix the format for DB

		date = ""
		date = parseDate(entry[3])
		while (date == ""):
			continue
		mytime = ""
		mytime = parseTime(entry[4])
		while (mytime == ""):
			continue

		purchaseTotal = PackageTotal[0]+PackageTotal[1]+PackageTotal[2]+PackageTotal[3]+PackageTotal[4]+wbTotal[0]+wbTotal[1]+gaTotal[0]+gaTotal[1]
	
		#print "Total", purchaseTotal
	
		# Check CC or cash
		#print "2"
		if (entry[2] != ""):
			purchaseType = 2
			#print "Credit Card, Purchase Amount =", entry[2], purchaseTotal
		else:
			purchaseType = 1
			#print "Cash, Purchase Amount =",purchaseTotal
		# Insert Purchase
		datetime = date + " " + mytime
		sql_insertPurchase(idkiosk, entry[0], purchaseTotal, datetime , purchaseType, purchaseStatus, cnx)
		# Load latest purchase Id
		purchaseId = sql_getPurchaseId(cnx)
		# Insert Credit Card or Cash Payment
		if (purchaseType == 1):
			CashInserted = entry[23].split(':')[0]
			BillsPaid = entry[22]
			LowerBillValue = entry[27]
			UpperBillValue = entry[26]
			try:
				LowerBillQuantity = entry[29]
			except:
				LowerBillQuantity = 0
			UpperBillQuantity = entry[28]
			HardMeter = getTotalCashInserted(entry[23])
			sql_insertCashPayment(purchaseId, CashInserted, BillsPaid, LowerBillValue, UpperBillValue, LowerBillQuantity, UpperBillQuantity, cnx)
			if ( HardMeter != 0 ):
				sql_updateHardMeter( idkiosk, HardMeter, cnx )
			
			# @Note check if this should be removed
			changeDispensed = getTotalChangeDispensed(entry[23])
			netGain = HardMeter - changeDispensed
			sql_insertCashMeter(purchaseId, HardMeter, changeDispensed, netGain, cnx)
		
				#print "Hard Meter updated"
			#print "CashPayment inserted"		sql_insertCashMeter(idPurchase, cashInserted, changeDispensed, netGain, cnx)
		if (purchaseType == 2):
			try:
				cardHolder = ""
			
				cardHolder = parseCardHolder(entry[6])
				while (cardHolder == ""):
					continue
			except:
				cardholder = "FAILED ENTRY"
			try:
				last4Numbers = entry[5][-4:]
			except:
				last4Numbers = "0000"
			try:
				receiptData = dh_processReceiptData(entry[1])
			except:
				receiptData = "MERCHANT ID: 000|CLERK ID: Undefined|SALE|NA  ************0000|ENTRY METHOD: CHIP|DATE: 01/01/2017 TIME: 19:19:08|INVOICE: 0000|REFERENCE: 0000|AUTH CODE: 0000|AMOUNT  USD$ 1.00|==========|TOTAL USD$ 1.00|This entry failed and was loaded as default|APPLICATION LABEL: VISA|AID: 0000|TVR: 0000|IAD: 0000|TSI: 0000|ARC: 00|"
			#print (entry[1])
		
			sql_insertCreditCardPayment(purchaseId, cardHolder, last4Numbers, cnx)
			#Separate these values for a prettier DB structure
			datetime = date + " " + mytime
			sql_insertReceiptData(purchaseId, datetime, receiptData, cnx)
			#print "CardPayment inserted"
	
		# Package, wristband and Gate Admission Update
		checkAndInsertPackages(purchaseId, PackageQ, PackageTotal, cnx)
		checkAndInsertWristBand(purchaseId, wbQ, wbTotal, WBCodes, cnx)
		checkAndInsertGateAdmission(purchaseId, gaQ, gaTotal, cnx)
		# Game Card Update
		creditsAdded = entry[21]
		if (creditsAdded != '0'):
			prevBalance = entry[24]
			newBalance = entry[25]
			sql_insertGameCard(purchaseId, creditsAdded, prevBalance, newBalance, cnx)
			#print "Game Card inserted"
	else:
		# Data version 2
		transactionNumber 	= entry[0]
		purchaseTotal 		= entry[2]
		
		date = ""
		date = parseDate(entry[3])
		while (date == ""):
			continue
		mytime = ""
		mytime = parseTime(entry[4])
		while (mytime == ""):
			continue
		datetime = date + " " + mytime
	
		# Check CC or cash
		if (entry[1] != "" and entry[1] != '0'):
			purchaseType = 2
			#print "Credit Card, Purchase Amount =", entry[2], purchaseTotal
		else:
			purchaseType = 1
			
		#print time, datetime
		sql_insertPurchase(idkiosk, transactionNumber, purchaseTotal, datetime , purchaseType, purchaseStatus, cnx)
		
		# Load latest purchase Id
		purchaseId = sql_getPurchaseId(cnx)
		print (purchaseId)
		# Insert Credit Card or Cash Payment
		if (purchaseType == 1):
			CashInserted = entry[23].split(':')[0]
			BillsPaid = entry[22]
			CassetteValue1 	= CheckCassetteEntry(entry[32])
			CassetteValue2 	= CheckCassetteEntry(entry[33])
			CassetteValue3 	= CheckCassetteEntry(entry[34])
			CassetteValue4 	= CheckCassetteEntry(entry[35])
			CassetteQty1   	= CheckCassetteEntry(entry[36])
			CassetteQty2   	= CheckCassetteEntry(entry[37])
			CassetteQty3   	= CheckCassetteEntry(entry[38])
			CassetteQty4	= CheckCassetteEntry(entry[39])

			sql_insertCashPayment2(purchaseId, CashInserted, CassetteValue1, CassetteValue2, CassetteValue3, CassetteValue4, CassetteQty1, CassetteQty2, CassetteQty3, CassetteQty4, cnx)
			HardMeter = entry[7]
			if ( HardMeter != 0 ):
				sql_updateHardMeter( idkiosk, HardMeter, cnx )
				#print "Hard Meter updated"

		if (purchaseType == 2):
			try:
				cardHolder = ""
			
				cardHolder = parseCardHolder(entry[6])
				while (cardHolder == ""):
					continue
			except:
				cardholder = "FAILED ENTRY"
			try:
				last4Numbers = entry[5][-4:]
			except:
				last4Numbers = "0000"
			try:
				receiptData = dh_processReceiptData(entry[1])
			except:
				receiptData = "MERCHANT ID: 000|CLERK ID: Undefined|SALE|NA  ************0000|ENTRY METHOD: CHIP|DATE: 01/01/2017 TIME: 19:19:08|INVOICE: 0000|REFERENCE: 0000|AUTH CODE: 0000|AMOUNT  USD$ 1.00|==========|TOTAL USD$ 1.00|This entry failed and was loaded as default|APPLICATION LABEL: VISA|AID: 0000|TVR: 0000|IAD: 0000|TSI: 0000|ARC: 00|"
			#print (entry[1])
		
			sql_insertCreditCardPayment(purchaseId, cardHolder, last4Numbers, cnx)
			sql_insertReceiptData(purchaseId, datetime, receiptData, cnx)
			#print "CardPayment inserted"


		CassetteValues = [entry[32], entry[33], entry[34], entry[35]]
		CassetteQtys   = [entry[36], entry[37], entry[38], entry[39]]
		
		ItemNames 	   = [entry[8], entry[9], entry[10], entry[11]]
		ItemPrices 	   = [entry[12], entry[13], entry[14], entry[15]]
		ItemQtys 	   = [entry[16], entry[17], entry[18], entry[19]]
		
		HopperNames    = [entry[20], entry[21], entry[22], entry[23]]
		HopperValues   = [entry[24], entry[25], entry[26], entry[27]]
		HopperQtys     = [entry[28], entry[29], entry[30], entry[31]]
		# Cassettes, Items and Hoppers
		
		checkAndInsertCassettes(purchaseId, CassetteValues, CassetteQtys, cnx)
		checkAndInsertItems(purchaseId, ItemNames, ItemPrices, ItemQtys, cnx)
		checkAndInsertHoppers(purchaseId, HopperNames, HopperValues, HopperQtys, cnx)
	###############################################################################
def insertDataEntry(idkiosk, entry, lineidx, machinetype, cnx):
	# Revisar Errores
	#purchase status 1-Completa, 2-hay errores
	#print len(entry)
	#pdb.set_trace()
	#print entry
	#print entry[3]
	#print entry[4]
	# wait for date and time to be calculated correctly
	# set date and wait
	# set time and wait
	#try:
	
	date = ""
	date = parseDate(entry[3])
	# Wait for parseDate to finish
	while (date == ""):
		continue
	mytime = ""
	mytime = parseTime(entry[4])
	# Wait for parseTime to finish
	while (mytime == ""):
		continue

	
	datetime = date + " " + mytime
	if (datetime == "0000-00-00 00:00:00"):
		print ">>>", lineidx
		#retry
		date = ""
		date = parseDate(entry[3])
		while (date == ""):
			continue
		mytime = ""
		mytime = parseTime(entry[4])
		while (mytime == ""):
			continue
		print datetime
	#print dh_processReceiptData(entry[1])

	## if the serial number and datetime are the same the transaction is already in the server
	evaluate = (sql_checkSerialNumber(entry[0], datetime, idkiosk, cnx) == False)

		
	# Version 2 data has 40 values, should work with this one 
	if (evaluate):
		if (entry [0] == '' and len(entry) >= 34):
			purchaseStatus = 1
			doStuff(idkiosk,entry, purchaseStatus, machinetype, cnx)
		elif (entry[0][0] != "E" and len(entry) >= 34):
			#print ("hola 2")
			purchaseStatus = 1
			doStuff(idkiosk,entry, purchaseStatus, machinetype, cnx)	
			
			#print "Inserted Sale:", entry[0], " With len:", len(entry)
		else:
			if (entry[0][0] == "E" and len(entry) >= 34):
				purchaseStatus = 2
				doStuff(idkiosk,entry, purchaseStatus, machinetype, cnx)
				#print "Inserted Incomplete Sale 1:", entry[0], " With len:", len(entry)
			
			elif (entry[0][0] == "E" and len(entry) >= 29):
				purchaseStatus = 2			
				doStuff(idkiosk,entry, purchaseStatus, machinetype, cnx)
				#print "Inserted Incomplete Sale 2:", entry[0], " With len:", len(entry)
			else:
				return 1
				print("Error: Entry format or data corrupted")
		#print "Data Entry inserted"
		
	else:
		#print( "Transaction already in the server, aborting line insertion" )
		return 1
	
	return 0

def getOffset(backupLines, newLines):
	pivot = 9
	offset = 0
	while (pivot > -1):
		for bkupLine in range(MAX_BKUP_LENGHT - 1, -1, -1):
			#print backupLines[bkupLine], "->", newLines[pivot]
			if (backupLines[bkupLine] == newLines[pivot]):
				return offset
		offset = offset + 1
		pivot = pivot - 1
	
	return offset

def txt_getFirstLine(filepath):
	pFile = open(filepath, 'r')
	result = []
	newLines = [line.rstrip('\n') for line in pFile]
	
	pathLen = len(newLines)
	if (pathLen == 0):
		print( "Error: First Line empty")
	else:
		result = newLines[0].split('\t')
		
	pFile.close()
	
	return result

def txt_getFullFile(filepath, backupLen, backupLines, skipFirst):
	pFile = open(filepath, 'r')
	result = []
	newLines = [line.rstrip('\n\r') for line in pFile]
	
	## if needed to skip first line newLines[1:]
	if (skipFirst == True):
		newLines = newLines[1:]
	newLines2 = [line.split('\t') for line in newLines]
	# revisar si el largo del nuevo es mayor que el backup
	# si es menor es archivo nuevo
	# si es igual hay que comparar la vara
	pathLen = len(newLines2)
	if (pathLen == 0):
		print( "Error: Empty file")
	elif (pathLen > backupLen):
		# esto funciona con archivo de 0 - 10 de tamanho
		for line in range(backupLen, pathLen):
			if (skipFirst != True):
				newLines2[line].pop()
			lenght = len(newLines2[line])
			if (skipFirst != True):
				if (lenght > 35):
					newLines2[line] = reduceDataEntry(newLines2[line], lenght)
			result.append(newLines2[line])
			
	elif (pathLen == backupLen and backupLen == MAX_BKUP_LENGHT):
		# Las listas son iguales, tenemos 10
		# revisar si los archivos son iguales
		# agregar las diferencias 
		offset = getOffset(backupLines, newLines2)
				
		if (offset > 0):
			#print("%d Line(s) to add " % offset )
			
			for line in range(MAX_BKUP_LENGHT - offset, MAX_BKUP_LENGHT):
				# Why am I popping the last char in the former format
				if (skipFirst != True):
					newLines2[line].pop()
				lenght = len(newLines2[line])
				print ("pathLen == backupLen and backupLen == MAX_BKUP_LENGHT")
				if (skipFirst != True):
					if (lenght > 35):
						newLines2[line] = reduceDataEntry(newLines2[line], lenght)
				result.append(newLines2[line])
		else:
			print( "No Entry Line to add")
	
	else:
		print( "Error: input file must be smaller or equal than 10 lines")
	
	pFile.close()

	return result
	
def loadDataToDB(idKiosk, datalist, machinetype, cnx):
	#print len(datalist)
	lineidx = 1
	aborted_lines = 0
	for line in datalist:
		lineidx = lineidx + 1
		if len(line) > 4:
			aborted_lines = aborted_lines + insertDataEntry(idKiosk, line, lineidx, machinetype, cnx)
		else:
			print ("Blank Line")
	return aborted_lines
	
""" Version 1: Machine type 1"""
# 0 - Transact Number (A)	12 - P1 Qty	(B)	.						24 - Previous Card Balance (E)
# 1 - CC Data (A)		    13 - P2 Qty	(B)	.						25 - New Card Balance (E)
# 2 - Purchase Amount (A)*	14 - P3 Qty	(B)	.						26 - Note Upper Value (D)
# 3 - Date (A)				15 - P4 Qty	(B)	.						27 - Note Lower Value (D)
# 4 - Time (A)				16 - P5 Qty	(B)	.						28 - Upper Bill Qty   (D)
# 5 - CreditCard[-4:] (C)	17 - Adult WB Cash Tot (F).				29 - Lower Bill Qty   (D)
# 6 - Card HolderName (C)	18 - Child WB Cash Tot (F).				30 - A GA Qty (H).
# 7 - P1 Price	(B)	.		19 - Adult WB Qty	(F).				31 - B GA Qty (H).
# 8 - P2 Price	(B)	.		20 - Child WB Qty	(F).				32 - A GA Total Cash (H).
# 9 - P3 Price	(B)	.		21 - Credits Added	(E)					33 - B GA Total Cash (H).
# 10 - P4 Price	(B)	.		22 - Bills Paid		(D)					34 - Wristband Code (F).
# 11 - P5 Price	(B)	.		23 - Cash Inserted:Hard Meter	(D)		35++ More Wristband Codes (F)

""" Version 2: Machine types 2, 3 and further"""
# 0 - Transaction Number 	    	20 - Hopper 1 Item Name	 
# 1 - CC Data 				    	21 - Hopper 2 Item Name
# 2 - Purchase Amount 		    	22 - Hopper 3 Item Name
# 3 - Date 					    	23 - Hopper 4 Item Name
# 4 - Time 					    	24 - Hopper 1 Value
# 5 - CreditCard[-4:] 		    	25 - Hopper 2 Value
# 6 - Card HolderName 			    26 - Hopper 3 Value
# 7 - Cash Inserted:Hard Meter   	27 - Hopper 4 Value
# 8 - Item 1 Name					28 - Hopper 1 Qty Dispensed
# 9 - Item 2 Name					29 - Hopper 2 Qty Dispensed
# 10 - Item 3 Name					30 - Hopper 3 Qty Dispensed
# 11 - Item 4 Name					31 - Hopper 4 Qty Dispensed
# 12 - Item 1 Price					32 - Cassette 1 Value
# 13 - Item 2 Price					33 - Cassette 2 Value
# 14 - Item 3 Price					34 - Cassette 3 Value
# 15 - Item 4 Price					35 - Cassette 4 Value
# 16 - Item 1 Qty					36 - Cassette 1 Bill Qty
# 17 - Item 2 Qty					37 - Cassette 2 Bill Qty
# 18 - Item 3 Qty					38 - Cassette 3 Bill Qty
# 19 - Item 4 Qty					39 - Cassette 4 Bill Qty



# got 41 items now (accomodate em)

# (A)* Si existe purchase amount significa que la compra es por credit card

# Siempre se inserta la compra Payment Type: 1 si es cash, 2 si es Credit Card
# consulta A - INSERT INTO `AmChangerReporter`.`Purchase` (`idPurchase`, `Kiosk_idKiosk`, `Number`, `CC Resp. Co`, `PurchaseAmount`, `Date`, `Time`, `PaymentType_idPaymentType`) VALUES ('4', '1', '4', '122', '20', '03/20/16', '01:11:00 PM', '2');

# consulta C - INSERT INTO `AmChangerReporter`.`CreditCardPayment` (`idCreditCard`, `Purchase_idPurchase`, `CreditCardHolder`) VALUES ('2', '2', '22424');
# Se inserta si hay Credit Card Payment
# consulta D - INSERT INTO `AmChangerReporter`.`CashPayment` (`idCashPayment`, `Purchase_idPurchase`, `CashInserted`, `BillsPaid`, `LowerBillValue`, `UpperBillValue`, `LowerBillQuantity`, `UpperBillQuantity`) VALUES ('1', '1', '21', '3', '1', '5', '1', '4');
# Se inserta si hay cash Payment

# consulta B - INSERT INTO `AmChangerReporter`.`Package` (`idPack`, `PackType_idPackType`, `PackQuantity`, `Purchase_idPurchase`) VALUES ('1', '1', '2', '1');
# si hay paquetes se inserta una vez por tipo de paquete


# consulta F - INSERT INTO `AmChangerReporter`.`Wristband` (`idWristband`, `Purchase_idPurchase`, `WristbandType_idWristbandType`, `WBQuantity`, `WBCashAmount`, `WBCodes`) VALUES ('3', '3', '1', '1', '20', 'fadwadawdwa');
# consulta G - INSERT INTO `AmChangerReporter`.`Wristband` (`idWristband`, `Purchase_idPurchase`, `WristbandType_idWristbandType`, `WBQuantity`, `WBCodes`) VALUES ('3', '3', '1', '2', '5454545');


# consulta E - INSERT INTO `AmChangerReporter`.`GameCard` (`idGameCard`, `CreditsAdded`, `PrevieusCardBalance`, `NewCardBalance`, `Purchase_idPurchase`) VALUES ('1', '21', '0', '21', '1');


# consulta H - INSERT INTO `AmChangerReporter`.`GateAdmission` (`idGate Admission`, `Gate AdmissionType`, `GATotalCash`, `GateAdmisionType_idGateAdmisionType`, `Purchase_idPurchase`) VALUES ('1', '1', '1', '1', '1');


def readInDirectory ():
	#fileList = os.listdir(DATA_PATH)
	fileList = [i for i in os.listdir(DATA_PATH) if os.path.isfile(os.path.join(DATA_PATH, i)) and (not i.startswith('.'))]
	return fileList

def readBackupDirectory ():
	
	fileList = os.listdir(BACKUP_PATH)
	return fileList
	

def main():
	cnx = connectToDatabase()
	#cambiar por un while para estar atento a nuevos archivos
	while (1):
		#cambiar por while, evitar salir del ciclo si aun hay archivos
		if (len(readInDirectory()) > 0):
				
			cnx = connectToDatabase()
			fileList = readInDirectory()
			# Leo el directorio de archivos de entrada
			# recorro la lista de archivos
			time.sleep(2)
			total = len(fileList)
			for idx in range(total):
				# reviso si el equipo tiene una entrada en la carpeta de backups  ### cuando borro el archivo en el backup?
				# si existe comparo los archivos y agrego a la DB los datos nuevos
				
				filePath = DATA_PATH + "/" + fileList[idx]
				backupFilePath = BACKUP_PATH + "/" + fileList[idx]
				quarantineFilePath = QUARANTINE_PATH + "/" + fileList[idx]
				debugFilePath = DEBUG_PATH + "/" + fileList[idx]
				print( "************ New Activity *************")
				print( "Time: %s " % datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'))
				print( "Entry File: %s " % fileList[idx])
				params = fileList[idx].split('|')
				#print params
				try:
					opName 		= params[0]
					kName 		= params[1]
					ip 			= params[2]
					if (len(params[3].split("-")) > 0):
						mac     = params[3].replace("-", ":")
					else:
						mac	= params[3]
					filedate    = params[4][0:4]+'-'+params[4][4:6]+'-'+params[4][6:8]+' '+params[4][8:10]+':'+params[4][10:12]+':'+params[4][12:14]

					#print (opName, kName, ip, mac)
				except:
					print "Filename not accepted, moving file to quarantine"
					ip = "None"
					mac = "None"
					shutil.copy2(filePath, quarantineFilePath)
					os.remove(filePath)
					continue
				try:
					machinetype	= int(params[5].split('.')[0])
				except:
					machinetype = 1
				
				"""
				Add mac address to kiosk table, add mac query, how to add mac to the kiosks?
				try:
					backupFilePath = BACKUP_PATH + "/" + ip + ".txt"
				except:
					print( "Filename Format Error")
					break	
				lenBackup = 0
				BackupLines2 = []
				
				try:
					pFile = open(backupFilePath, 'r')
					BackupLines = [line.rstrip('\n') for line in pFile]
					BackupLines2 = [line.split('\t') for line in BackupLines]
					lenBackup = len(BackupLines2)
					pFile.close()
					print( "Backup exists with lenght %d " % lenBackup)
					
				except:
					#shutil.copy2(filePath, backupFilePath)
					print( "No Backup found, need to create one")
				"""
				lenBackup = 0
				BackupLines2 = []
				# Signal to skip first line in case the file comes with machine status
				skipFirst = False
				# debe revisar primero si esta en el server y luego 
				# preguntar y crear el backup
				#print( "IP: %s" % ip )
				# ID = sql_getKioskIdbyIpAndMac(ip, mac, cnx)
				# check if the kiosk mac is inserted and add it
				
				## Special case for testing purposes
				## Operator must be inserted on the system, otherwise it will give errors
				if (mac == 'Not Inserted'):
					try:
						OpId = sql_getOperatorIdByName(opName, cnx)
						kID = sql_getKioskIdByNameAndOpId(kName, OpId, cnx)
						if (kID != "None"):
							sql_editKioskMacAddress(kID, mac, cnx);
							print "Mac Address updated"
						else:
							sql_insertNewKioskWithMac(opName, kName, "", ip, "", mac, machinetype, cnx)
							print "New Kiosk Inserted"
					except:
						kID = "None"
				else:
					kID = sql_getKioskIdbyMac(mac, cnx)

				if (kID == "None"):
					OpId = sql_getOperatorIdByName(opName, cnx)
					#print OpId
					if (OpId != "None"):
						kID = sql_getKioskIdByNameAndOpId(kName, OpId, cnx)
						print (kID, kName, OpId)
						if (kID != "None"):
							sql_editKioskMacAddress(kID, mac, cnx);
							print "Mac Address updated"
						else:
							sql_insertNewKioskWithMac(opName, kName, "", ip, "", mac, machinetype, cnx)
							print "New Kiosk Inserted"
					else:
						kID = "None"
				
				## if kID is still not found exit operation
				if (kID == "None"):
					time.sleep(0.1)
					print( "This Operator does not exist in the server, moving file to quarantine")
					shutil.copy2(filePath, quarantineFilePath)
					os.remove(filePath)
				else:
					
					typeCheck = sql_getMachineTypeById(kID, cnx)
					if (machinetype != typeCheck):
						time.sleep(0.1)
						print( "Machine Type not compatible with server data, moving file to quarantine")
						shutil.copy2(filePath, quarantineFilePath)
						os.remove(filePath)
						continue
					## If the machine is not a kiosk update the machine status 
					## and signal the list parser to skip the first line
					if ( machinetype != 1):
						collectAndInsertMachineStatus(kID, filePath, filedate, cnx)
						skipFirst = True
					#maybe this is not checking for correct data	

					#pdb.set_trace()
					aborted_lines = 0
					lista = txt_getFullFile(filePath, lenBackup, BackupLines2, skipFirst)
					#print len(lista)
					
					if len(lista) > 0:
						## Insert purchase lines, count how much are aborted to report
						aborted_lines = loadDataToDB(kID, lista, machinetype, cnx)
						
						#print( "Data added to Kiosk" )
						shutil.copy2(filePath, backupFilePath)
						#print( "Backup Updated" )
						#print("done")
					else:
						print( "No new data could be found" )
					#shutil.copy2(filePath, debugFilePath)
					os.remove(filePath)
					
					print ("Total Lines", len(lista) )
					print ("Aborted Lines", aborted_lines)
					print( "Input File Removed" )
					
				time.sleep(0.1)
				
			cnx.close()
			#time.sleep(0.5)
		else:
			cnx.close()
			time.sleep(20)
			
if __name__ == "__main__":
    main()
#
#
#print "T16 ready"
